# JyGame
- ## unity 前端游戏框架！
1. 框架通过继承 MonoBehaviour 的 GameApp 启动和关闭。
2. 框架内有许多独立模块，比如：
    * [事件模块](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/Event)(负责事件的分发工作) (已经完成)
    * [ui](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/UI)(独立的mvc构架) (基于UGUI的框架已经完成)
    * [网络](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/Network)(独立的客户端tcp、http、udp连接等)(基于protbuf3.6.1的网络架构已经完成)
    * [数据](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/Data)(数据库、读表数据等)
    * [资源](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/Res)(热更新资源、加载、卸载等) (assetbundle生成、加载、管理已经完成)
    * [脚本](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/Script)(Lua等热更新)
    * [时间](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/Time)(同步服务器时间)
    * [对象池](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/Object)(管理全局对象、视野管理等) (有限池、无限池已经完成)
    * [日志](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/Log)(手机日志存储、上传云日志等) (日志打印管理功能已经完成,存储和上传服务器还未完成)
    * [AI](https://gitee.com/chenbojun/JyGame/tree/master/Assets/FrameworkCore/Modules/AI)(一般寻路<1.A*寻路>、<2.navmesh>、FSM等)等。
3. 每个模块负责自己模块内的管理，模块跟随系统的生命周期。
    * 这样做的好处是：
        * 每个模块之间独立开发，更加容易维护和增加新模块。
        * 满足程序设计的开闭原则。
        * 满足程序设计的单一职责。
        * 满足程序设计的接口隔离原则。
        * 满足程序设计的迪米特原则。
4. 最后通过外观模式或者其他方法集合常用功能<开发中>。
