﻿/*
 *  date: 2018-05-29
 *  author: John-chen
 *  cn: 脚本管理器
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 脚本管理器
    /// </summary>
    public class ScriptModule : BaseModule
    {
        public ScriptModule(string name = ModuleName.Script) : base(name)
        {

        }

        public override void InitGame()
        {
            
        }
    }
}
